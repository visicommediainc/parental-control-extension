__namespace__.$api = {
    version: chrome.app.getDetails().version,
    updateInterval: 25 * 60 * 1000, //25 mins
    beatInterval: 24 * 60 * 60 * 1000, // 1 day
    beatLastUpdate: 0,
    blockHost: chrome.extension.getURL('html/blocker.html?url='),
    blockUser: chrome.extension.getURL('html/blockUser.html?url='),
    baseUrl: "http://urlfilter2.vmn.net/safeweb/tracker/adultfilter/all.list.br",
    ignore_urls: [
        "chrome://",
        "chrome://extensions",
        "chrome-extension:",
        "chrome-search://",
        "chrome-devtools://",
        "chrome.google.com",
        "about:",
        "view-source:",
        "file://",
        "//localhost",
        /analytics.*/
    ],
    controller: {
        adult: {
            words: [],
            sites: [],
            domains: []
        },
        tracker: {
            $ads: [],
            $social: [],
            $analytics: [],
            $sponsors: [],
            $spyware: []
        }
    },
    checkUrl: function(url) {
        if (!url || url === "") return false;
        if (url.indexOf(__namespace__.$api.blockHost) !== -1) {
            if (url.match(/url=*([^\n]*)/).length === 2) {
                return true;
            }
        }
        for (var i = 0, j = __namespace__.$api.ignore_urls.length; i < j; i++) {
            if (url.indexOf(__namespace__.$api.ignore_urls[i]) > -1) return false;
        }
        return true;
    },
    getData: function() {
        __namespace__.$utils.getRemoteInfo(__namespace__.$api.baseUrl, function(byteArray) {
            try {
                var content = __namespace__.$utils.bin2String(byteArray);
                reply = content.split("-------------");
                if (reply.length !== 4 && reply.length !== 3)
                    throw { message: "ERR_CORRUPTED_FEED" };
                __namespace__.$api.controller.adult.sites = reply[0].split('\n');
                chrome.storage.local.set({ "sites": __namespace__.$api.controller.adult.sites }, function() {
                    console.log("sites saved at " + new Date());
                });
                __namespace__.$api.controller.adult.words = reply[1].split('\n');
                chrome.storage.local.set({ "words": __namespace__.$api.controller.adult.words }, function() {
                    console.log("words saved at " + new Date());
                });
                __namespace__.$api.controller.adult.domains = reply[2].split('\n');
                chrome.storage.local.set({ "domains": __namespace__.$api.controller.adult.domains }, function() {
                    console.log("domains saved at " + new Date());
                });
                if (reply[3]) {
                    __namespace__.$api.controller.tracker.$ads = reply[3].split('\n');
                    chrome.storage.local.set({ "ads": __namespace__.$api.controller.tracker.$ads }, function() {
                        console.log("ads saved at " + new Date());
                    });
                }
                var now = parseInt(new Date().getTime() / 1000);
                chrome.storage.local.set({ "dbLastUpdate": now });
            } catch (e) {
                console.log("something went wrong here...not updating trackers.error:" + e.message);
            }
        }, 'arraybuffer');
    },
    load: function() {
        var now = parseInt(new Date().getTime() / 1000);
        chrome.storage.local.get("beatLastUpdate", function(result) {
            __namespace__.$api.beatLastUpdate = (typeof result.beatLastUpdate !== "undefined") ? result.beatLastUpdate : 0;
            beatUpdateDue = parseInt(__namespace__.$api.beatLastUpdate) + parseInt(__namespace__.$api.beatInterval / 1000);
            if (beatUpdateDue > now) {
                console.log("Too soon to send heart beat!");
                return;
            }
            setTimeout(function() {
                __namespace__.$analytics.trackEvent("Heartbeat_DailyUser", __namespace__.$initialize.campaignId, " ", " ");
            }, 1000);
            chrome.storage.local.set({ "beatLastUpdate": now }, function() {
                __namespace__.$api.beatLastUpdate = now;
                console.log("beatLastUpdate sent at " + now);
            });

        });
        chrome.storage.local.get("dbLastUpdate", function(result) {
            __namespace__.$api.dbLastUpdate = (typeof result.dbLastUpdate !== "undefined") ? result.dbLastUpdate : 0;
            if (__namespace__.$api.dbLastUpdate === 0) {
                __namespace__.$api.getData();
            } else {
                chrome.storage.local.get("sites", function(result1) {
                    __namespace__.$api.controller.adult.sites = result1.sites;
                });
                chrome.storage.local.get("words", function(result2) {
                    __namespace__.$api.controller.adult.words = result2.words;
                });
                chrome.storage.local.get("domains", function(result3) {
                    __namespace__.$api.controller.adult.domains = result3.domains;
                });
                chrome.storage.local.get("ads", function(result4) {
                    __namespace__.$api.controller.tracker.$ads = result4.ads;
                });
                chrome.storage.local.get("social", function(result5) {
                    __namespace__.$api.controller.tracker.$social = result5.social;
                });
                chrome.storage.local.get("analytics", function(result6) {
                    __namespace__.$api.controller.tracker.$analytics = result6.analytics;
                });
                chrome.storage.local.get("sponsors", function(result7) {
                    __namespace__.$api.controller.tracker.$sponsors = result7.sponsors;
                });
                chrome.storage.local.get("spyware", function(result8) {
                    __namespace__.$api.controller.tracker.$spyware = result8.spyware;
                });

                trackerUpdateDue = parseInt(__namespace__.$api.dbLastUpdate) + parseInt(__namespace__.$api.updateInterval / 1000);
                if (trackerUpdateDue > now) {
                    console.log("Too soon to update tracking list " + parseInt(trackerUpdateDue - now) + " secs left. Keeping old list");
                    return;
                }
                __namespace__.$api.getData();
            }
        });

    }
};
document.addEventListener("DOMContentLoaded", function() {
    __namespace__.$api.load();
    setInterval(function() { __namespace__.$api.load(); }, __namespace__.$api.updateInterval);
});