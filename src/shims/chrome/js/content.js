var currenturl = window.location.href;
var __namespace__ = {
    injectCSS: function() {
        var style = document.createElement('link');
        style.rel = 'stylesheet';
        style.type = 'text/css';
        style.href = chrome.extension.getURL('css/content.css');
        (document.head || document.documentElement).appendChild(style);
    },
    getDomain: function(url) {
        if (url === null)
            return url;
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            url = match[2];
        }
        subs = url.split(">");
        if (subs.length > 2) {
            return subs[1] + "." + subs[2];
        } else {
            return url;
        }
    },
    getParameterByName: function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    tempMask: function(is_blockable) {
        var id = "__namespace__";
        if (is_blockable === 'true' || is_blockable === true)
            mask = 'true';
        else
            mask = 'none';

        //alert('mask:' + mask);
        if (document.getElementById(id)) {
            blockerOverlay = document.getElementById(id);
            blockerOverlay.style.display = mask;
        } else {
            blockerOverlay = document.createElement("div");
            blockerOverlay.innerHTML = '<div id="' + id + '" style="position:fixed;width:100%;height:100%;background-color:#EFE915;display:' + mask + ';z-index:1000000;top:0;left:0;opacity:0.99;"><div id="divid_inner98sXja" style="position:fixed;top:20%;left:35%;background-color:white;color:black;padding:20px;width:30%;height:40%;font-size:16px;text-align:center;vertical-align:middle;font-family:monospace; cursor:pointer;"><h1 style="color:black;margin: 0; margin-bottom: 10px; padding 0; font-size:30px;text-align:center;padding-bottom:10px;font-family:monospace;">' + ((is_blockable) ? "Blocked!" : "") + '</h1><br>' + ((is_blockable) ? "This page has been temporarily blocked because it may <b>contains adult content</b>, which is not allowed on this computer." : "") + '</div></div><style>#divid_inner98sXja:hover{color:#000;background:white;background-color:white;}</style>';
            document.body.appendChild(blockerOverlay);
        }
    },
    checkTitle: function() {
        title = document.title;
        //console.log("title:" + title);
        if (title.length > 0) {
            chrome.extension.sendMessage({ message: "keywords", value: title, url: currenturl });
        }
    },
    checkDisclaimer: function() {
        var disclaimerCl = document.getElementsByClassName("disclaimer");
        var disclaimerId = document.getElementById("disclaimer");

        if (disclaimerId) {
            if (disclaimerId.innerText.indexOf("18 years or older") !== -1 ||
                disclaimerId.innerText.indexOf("18+") !== -1
            ) {
                chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "disclaimer" });
            }
        }
        if (disclaimerCl && disclaimerCl.length > 0) {
            if (disclaimerCl[0].innerText.indexOf("18 years or older") !== -1 ||
                disclaimerCl[0].innerText.indexOf("18+") !== -1
            ) {
                chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "disclaimer" });
            }
        }
    },
    checkAgeRestriction: function() {
        var query = "//body[";
        query += "contains(., 'age-restricted')";
        query += " or ";
        query += "contains(., 'Warning: This Site Contains Sexually Explicit Content')";
        query += " or ";
        query += "contains(., ' contains adult material')";
        query += " or ";
        query += "contains(., 'website contains adult material')";
        query += " or ";
        query += "contains(., 'ADULTS ONLY DISCLAIMER')";
        query += " or ";
        query += "contains(., 'Warning: You must be 18 years or older')";
        query += " or ";
        query += "contains(., 'adult-only')";
        query += "]";
        var headings = document.evaluate(query, document, null, XPathResult.ANY_TYPE, null);
        var thisHeading = headings.iterateNext();
        if (thisHeading) {
            textContent = thisHeading.textContent;
            if (textContent) {
                textContent = textContent.toLowerCase();
                if (textContent.indexOf("porn") !== -1) {
                    chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "age-restriced" });
                }
            }
        }
    },
    checkMetaTags: function() {
        var metaTags = document.querySelectorAll("meta");
        for (var i = 0; i < metaTags.length; i++) {
            var meta = metaTags[i];
            var key = meta.attributes[0].nodeValue.toLowerCase();
            switch (key) {
                case 'keywords':
                case 'description':
                case 'og:description':
                case 'og:title':
                    {
                        //console.log("attr.content"+ meta.attributes.getNamedItem("content").value);
                        value = String(meta.attributes.getNamedItem("content").value).toLowerCase();
                        if (value.length === 0)
                            return;
                        //console.log("keywords:" + value);
                        chrome.extension.sendMessage({ message: "keywords", value: value, url: currenturl });
                    }
                    break;
                case 'pics-label':
                    {
                        value = meta.attributes[1].nodeValue.toLowerCase();
                        ratings = value.split("r (")[1].split(")")[0].split(" ");
                        for (var k = 0; k < ratings.length; k++) {
                            label = ratings[k];
                            if (label === "n" || label == "s") {
                                rating = parseInt(ratings[k + 1]);
                                if (rating > 0) {
                                    chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "adult-content-tag" });
                                }
                            }
                        }
                    }
                    break;
                case 'rating':
                    {
                        value = meta.attributes[1].nodeValue.toLowerCase();
                        if (value.indexOf("rta") !== -1) {
                            chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "RTA tag" });
                        }
                    }
                    break;
                case 'adult_content':
                    {
                        value = meta.attributes[1].nodeValue.toLowerCase();
                        if (value.indexOf("mature") !== -1 || value.indexOf("true") !== -1 || value.indexOf("restriced")) {
                            chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "adult-content-tag" });
                        }
                    }
                    break;
            }
        }
    },
    checkAdultAds: function() {
        var images = document.querySelectorAll("img");
        if (images.length === 0)
            return;
        var query = "//body[";
        query += "contains(., 'exoclick')";
        query += " or ";
        query += "contains(., 'xxximagetpb')";
        query += " or ";
        query += "contains(., 'juicyads')";
        query += " or ";
        query += "contains(., 'eroticads.com')";
        query += "]";
        var headings = document.evaluate(query, document, null, XPathResult.ANY_TYPE, null);
        var thisHeading = headings.iterateNext();
        if (thisHeading) {
            textContent = thisHeading.textContent;
            if (textContent) {
                textContent = textContent.toLowerCase();
                if (textContent.indexOf("porn") !== -1) {
                    chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "erotic-ads" });
                }
            }
        }
    },
    checkAds: function() {
        var data_asyncs = document.querySelectorAll('*[data-cfasync=false]');
        if (data_asyncs.length > 0) {
            for (var i = 0; i < data_asyncs.length; i++) {
                data_async = data_asyncs[0];
                console.log("data_async=" + data_async.src);
                data_async.innerText = '';
                data_async.removeAttribute('src');
            }
        }
        var scripts = document.scripts;
        for (var j = 0; j < scripts.length; j++) {
            var script = scripts[j];
            if (script.innerText.indexOf("popunder") !== -1) {
                script.innerText = "";
            }
        }
    },
    checkPopunder: function() {
        if (typeof popunder !== "undefined") {
            popuder = {};
            console.log(203);
        }
        if (typeof _wm_settings !== "undefined") {
            _wm_settings = {};
            console.log(207);
        }
    },
    init: function() {
        var domain = __namespace__.getDomain(currenturl);
        chrome.extension.onMessage.addListener(__namespace__.onMessage);
        chrome.extension.sendMessage({ message: "isblock_css", domain: domain }, function(response) {
            options = response.options;
            tracker = response.tracker;
            if (options.filters.$ads === 1 && (!tracker || tracker.blocksite == 1)) {
                __namespace__.injectCSS();
            }
            //adult filter here;
            if (options.filters.$adult === 1) {
                if (currenturl.indexOf("blocker.html") !== -1 ||
                    domain.indexOf("google") !== -1 ||
                    domain.indexOf("bing") !== -1 ||
                    domain.indexOf("youtube") !== -1 ||
                    domain.indexOf("yahoo") !== -1) {
                    currenturl = __namespace__.getParameterByName('url');
                    return;
                }
                if (domain.indexOf("porn") !== -1 ||
                    domain.indexOf("sex") !== -1 ||
                    domain.indexOf("xxx") !== -1 ||
                    domain.indexOf("slut") !== -1) {
                    __namespace__.tempMask(true);
                    chrome.runtime.sendMessage({ message: "iswhite", url: currenturl }, function(response) {
                        if (response) {
                            __namespace__.tempMask(false);
                        } else {
                            chrome.extension.sendMessage({ message: "blocksite", url: currenturl, reason: "adult domain" });
                        }
                    });
                }
                __namespace__.checkTitle();
                __namespace__.checkDisclaimer();
                __namespace__.checkAgeRestriction();
                __namespace__.checkMetaTags();
                __namespace__.checkAdultAds();
            }
            if (options.filters.$ads === 1) {
                __namespace__.checkAds();
                __namespace__.checkPopunder();
            }
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    __namespace__.init();
}, true);