Array.prototype.unique = function() {
    var n = {},
        r = [];
    for (var i = 0; i < this.length; i++) {
        if (!n[this[i]]) {
            n[this[i]] = true;
            r.push(this[i]);
        }
    }
    return r;
};

Object.prototype.length = function() {
    var count = -1;
    for (var i in this) count++;
    return count;
};