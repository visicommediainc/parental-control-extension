function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function setUrl() {
    var url = getParameterByName('url');
    if (url !== null || url !== "") {
        document.getElementById("url").innerHTML = url;
    }
}

function accept() {
    if (document.getElementById("check").className == "checkbox") {
        document.getElementById("check").className = "checkbox-checked";
        document.getElementById("btnContinueDiv").className = "hover-bar-2";
    } else {
        document.getElementById("check").className = "checkbox";
        document.getElementById("btnContinueDiv").className = "hover-bar-2 disabled";
    }
}

function backToSafety() {
    document.location.href = "https://mystart.com/?pr=vmn&id=pcontrol";
}

function setLocation() {
    window.location.href = "http://" + url;
}

$(document).ready(function() {
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
    setUrl();
    document.getElementById("check").onclick = function() {
        accept();
    };
    document.getElementById("backtosafe").onclick = function() {
        backToSafety();
    };

    document.getElementById("btnContinueDiv").onclick = function() {
        if (confirm("Do you want to access this site?")) {
            var url = getParameterByName('url');

            chrome.extension.sendMessage({
                message: "white_list",
                "url": url
            }, function(response) {
                if (response == 1) {
                    window.setTimeout(setLocation, 100);
                }
            });
        }
    };
});