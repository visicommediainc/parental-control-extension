if (!__namespace__) var __namespace__ = {};

__namespace__.$settings = {
    options: {
        filters: {
            $master: 1,
            $adult: 1,
            $ads: 1,
            $block: 1,
            $trust: 1
        },
        forcesafe: {
            bing: 1,
            google: 1,
            yahoo: 1,
            youtube: 1
        },
        search: {
            bing: 1,
            google: 1,
            yahoo: 1,
            suspicious: 1
        },
        account: {
            require: 0,
            block: 0,
            trust: 0
        }
    },
    backupOptions: null,
    UNINSTALL_URL: "__uninstallurl__?cid=",
    INSTALLED_URL: "html/options.html?message=1",
    setOptions: function(options) {
        __namespace__.$settings.options = options;
        chrome.storage.local.set({
            options: __namespace__.$settings.options
        }, function() {});
    },
    setBackupOptions: function(options) {
        __namespace__.$settings.backupOptions = options;
        chrome.storage.local.set({
            backupOptions: options
        }, function() {});
    },
    setOption: function(category, name, value) {
        __namespace__.$settings.options[category][name] = value;
        chrome.storage.local.set({
            options: __namespace__.$settings.options
        }, function() {});
        //console.log("setoption....:");
        //console.log(__namespace__.$settings.options);
    },
    set: function(obj) {
        chrome.storage.local.set(obj, function() {});
    },
    get: function(key, callback) {
        chrome.storage.local.get(key, function(result) {
            callback(result);
        });
    }
};
chrome.storage.local.get("options", function(result) {
    try {
        if (typeof result.options == "undefined") {
            chrome.storage.local.set({
                options: __namespace__.$settings.options
            }, function() {});
        } else {
            __namespace__.$settings.options = result.options;
        }
    } catch (ex) {}
});

chrome.storage.local.get("backupOptions", function(result) {
    try {
        if (typeof result.backupOptions == "undefined") {
            chrome.storage.local.set({
                backupOptions: __namespace__.$settings.options
            }, function() {});
        } else {
            __namespace__.$settings.backupOptions = result.backupOptions;
        }
    } catch (ex) {}
});