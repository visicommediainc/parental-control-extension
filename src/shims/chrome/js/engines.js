__namespace__.$engines = {
    options: {},
    generateIcon: function(url, isPorn, issuspicious) {
        var icons = {
            allow: chrome.extension.getURL('images/icons/19g.png'),
            block: chrome.extension.getURL('images/icons/19r.png'),
            suspect: chrome.extension.getURL('images/icons/19y.png')
        };
        if (!isPorn) {
            if (!issuspicious) {
                return '<div class="__namespace___item" title="SAFE" style="background:url(' + icons.allow + ') no-repeat;">&nbsp;</div>';
            } else {
                return '<div class="__namespace___item" title="SUSPICIOUS" style="background:url(' + icons.suspect + ') no-repeat;">&nbsp;</div>';
            }
        } else {
            return '<div class="__namespace___item" title="NOT SAFE" style="background:url(' + icons.block + ') no-repeat;">&nbsp;</div>';
        }

    },
    responseFunc: function(response) {
        if (!response)
            return;
        //console.log("response:" + JSON.stringify(response));
        className = response.anchor;
        index = response.linkIndex;
        elm = document.querySelectorAll(className)[index];
        var link = elm;
        if (link.tagName != "A")
            link = elm.querySelector("a");
        if (link.innerHTML.indexOf("__namespace__") !== -1)
            return;
        isPorn = response.isPorn;
        issuspicious = response.issuspicious;
        var iconDiv = __namespace__.$engines.generateIcon(link.href, isPorn, issuspicious);
        orig = link.innerHTML;
        link.innerHTML = orig + iconDiv;
        if (isPorn || (issuspicious && __namespace__.$engines.options.search.suspicious === 1)) {
            var hook = (link.href.indexOf("?") !== -1) ? "&" : "?";
            var url = link.href + hook + "ptbdw=1";
            link.setAttribute("href", url);
            //link.setAttribute("dirtyhref", url);
        }
    },
    injectCSS: function() {
        var style = document.createElement('link');
        style.rel = 'stylesheet';
        style.type = 'text/css';
        style.href = chrome.extension.getURL('css/engines.css');
        (document.head || document.documentElement).appendChild(style);
    },
    inject: function(search) {
        switch (search) {
            case "bing":
                {
                    className = ".b_algo";
                    subclass = ".b_algo";
                    query = "b_caption";
                }
                break;
            case "google":
                {
                    className = ".r";
                    subclass = ".r";
                    query = ".s";
                }
                break;
            case "yahoo":
                {
                    className = ".yschttl";
                    subclass = ".algo";
                    query = "lh-18";
                }
                break;
        }
        elms = document.querySelectorAll(className);
        if (elms.length === 0) {
            elms = document.querySelectorAll(subclass);
            className = subclass;
        }
        if (elms.length > 0) {
            //console.log("start scanning " + search);
            //console.log("links.length=" + elms.length);
            for (var i = 0; i < elms.length; i++) {
                var anchor = elms[i];
                if (elms[i].tagName != "A")
                    anchor = elms[i].querySelector("a");
                var label = anchor.innerText;
                var desc = elms[i].parentNode.querySelectorAll(query);
                if (desc && desc.length > 0) {
                    label += " " + desc[0].innerText;
                }
                //console.log("anchor:" + anchor.href);
                chrome.extension.sendMessage({
                    message: "scanlink",
                    anchor: className,
                    url: anchor.href,
                    label: label,
                    linkIndex: i
                }, __namespace__.$engines.responseFunc);
            }
        }
    },
    init: function() {
        var domain = __namespace__.getDomain(currenturl);
        if (domain.indexOf("yahoo.") == -1 &&
            domain.indexOf("bing.com") == -1 &&
            domain.indexOf("google.") == -1) {
            return;
        }
        chrome.extension.sendMessage({ message: "get_options", domain: domain }, function(options) {
            var search = "";
            if (domain.indexOf("yahoo.") != -1)
                search = "yahoo";
            else if (domain.indexOf("bing.com") != -1)
                search = "bing";
            else if (domain.indexOf("google.") != -1)
                search = "google";
            if ((search === "bing" && options.search.bing === 0) ||
                (search === "google" && options.search.google === 0) ||
                (search === "yahoo" && options.search.yahoo === 0)) {
                return false;
            }
            __namespace__.$engines.options = options;
            __namespace__.$engines.injectCSS();
            mark = "__namespace___item";
            marker = document.querySelector(mark);
            if (marker) {
                return;
            }
            __namespace__.$engines.inject(search);
            marker = document.createElement("div");
            marker.id = mark;
            document.body.appendChild(marker);
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    __namespace__.$engines.init();
}, false);