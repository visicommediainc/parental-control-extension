__namespace__.$adult = {
    activeTabId: -1,
    white: [],
    black: [],
    matchingWords: [],
    blackWords: [],
    checkCookies: function(cookies) {
        for (var i = 0; i < cookies.length; ++i) {
            if (cookies[i].name === "PREF") {
                value = cookies[i].value;
                if (value.indexOf("f2=") === -1) {
                    value += "&f2=8000000";
                    chrome.cookies.set({ url: "https://.youtube.com/", name: "PREF", value: value });
                    chrome.tabs.reload(__namespace__.$adult.activeTabId);
                }
            }
        }
    },
    addWhite: function(url) {
        for (var i = 0; i < __namespace__.$adult.white.length; i++) {
            if (__namespace__.$adult.white[i] === url)
                return;
        }
        __namespace__.$adult.white.push(url);
        __namespace__.$settings.set({ "white": __namespace__.$adult.white });
    },
    removeWhite: function(url) {
        for (var i = 0; i < __namespace__.$adult.white.length; i++) {
            if (__namespace__.$adult.white[i] === url) {
                __namespace__.$adult.white.splice(i, 1);
                __namespace__.$settings.set({ "white": __namespace__.$adult.white });
                return;
            }
        }
    },
    isWhiteSite: function(url) {
        if (__namespace__.$settings.options.filters.$trust === 0) {
            return false;
        }
        for (var j = 0; j < __namespace__.$adult.white.length; j++) {
            if (url.search(__namespace__.$adult.white[j]) !== -1) {
                return true;
            }
        }
        return false;
    },
    addBlack: function(url) {
        for (var i = 0; i < __namespace__.$adult.black.length; i++) {
            if (__namespace__.$adult.black[i] === url)
                return;
        }
        __namespace__.$adult.black.push(url);
        __namespace__.$settings.set({ "black": __namespace__.$adult.black });
    },
    addBlackWords: function(keyword) {
        for (var i = 0; i < __namespace__.$adult.blackWords.length; i++) {
            if (__namespace__.$adult.blackWords[i] === keyword)
                return;
        }
        __namespace__.$adult.blackWords.push(keyword);
        __namespace__.$settings.set({ "blackwords": __namespace__.$adult.blackWords });
    },
    removeBlack: function(url) {
        for (var i = 0; i < __namespace__.$adult.black.length; i++) {
            if (__namespace__.$adult.black[i] === url) {
                __namespace__.$adult.black.splice(i, 1);
                __namespace__.$settings.set({ "black": __namespace__.$adult.black });
                return;
            }
        }
        for (var j = 0; j < __namespace__.$adult.blackWords.length; j++) {
            if (__namespace__.$adult.blackWords[j] === url) {
                __namespace__.$adult.blackWords.splice(j, 1);
                __namespace__.$settings.set({ "blackWords": __namespace__.$adult.blackWords });
                return;
            }
        }

    },
    isBlackSite: function(url) {
        if (__namespace__.$settings.options.filters.$block === 0) {
            return false;
        }
        for (var j = 0; j < __namespace__.$adult.black.length; j++) {
            if (url.search(__namespace__.$adult.black[j]) !== -1) {
                return true;
            }
        }
        return false;
    },
    getSuffix: function(domain) {
        if (domain === null)
            return domain;

        idx = domain.lastIndexOf(".");
        suffix = domain.substring(idx);
        return suffix;
    },
    isSuspiciousDomain: function(request) {
        var url = request.url;
        var matching = __namespace__.$utils.filterItem(__namespace__.$api.controller.adult, "domains", url);
        if (matching.value.length > 0) {
            return true;
        }
        return false;
    },
    isSuspiciousWord: function(word) {
        if (word.length === 0)
            return false;
        matchingWords = [];

        var matching = __namespace__.$utils.filterItem(__namespace__.$api.controller.adult, "words", word, true);
        if (matching.value.length > 0) {
            matchingWords = matchingWords.concat(matching.value);
            matchingWords = matchingWords.unique();
        }
        if (matchingWords.length > 1) {
            return true;
        }
        return false;
    },
    isPornSite: function(request) {
        if (__namespace__.$settings.options.filters.$adult === 0) {
            return false;
        }
        if (!request || request.tabId === -1 || !__namespace__.$api.checkUrl(request.url)) {
            return false;
        }
        var url = request.url;
        if (url.search("blocker.html") !== -1 || url.search("url=") !== -1) {
            return false;
        }
        if (url.search("ptbdw=1") !== -1) {
            return true;
        }

        //if (__namespace__.$settings.options.filters.$block == 1 && __namespace__.$adult.isBlackSite(url)) {
        //    return true;
        //}

        for (var i = 0; i < __namespace__.$adult.white.length; i++) {
            if (url.search(__namespace__.$adult.white[i]) !== -1) {
                return false;
            }
        }
        var domain = __namespace__.$utils.getDomain(url);
        if (__namespace__.$adult.getSuffix(domain) === ".xxx" ||
            __namespace__.$adult.getSuffix(domain) === ".porn" ||
            __namespace__.$adult.getSuffix(domain) === ".sex" ||
            __namespace__.$adult.getSuffix(domain) === ".sexy")
            return true;

        var matching = __namespace__.$utils.filterItem(__namespace__.$api.controller.adult, "sites", url);
        if (matching.value.length > 0) {
            return true;
        }
        matching = __namespace__.$utils.filterItem(__namespace__.$api.controller.adult, "domains", url);
        if (matching.value.length > 0) {
            return true;
        }
        return false;
    },
    blockSite: function(request, reason) {
        if (__namespace__.$settings.options.filters.$adult === 0)
            return false;
        var url = request.url;
        var tabId = request.tabId;
        var tracker = __namespace__.$browser.findTrackerByTabId(tabId);
        if (tracker) {
            tracker.adult = 1;
        }
        if ((url.indexOf("blocker.html") !== -1 || url.indexOf("blockUser.html") !== -1 && url.indexOf("url=") !== -1) ||
            url.indexOf(__namespace__.$api.blockHost) !== -1) {
            return false;
        }
        var domain = __namespace__.$utils.getDomain(url);
        if (reason !== "user") {
            newUrl = __namespace__.$api.blockHost + domain;
        } else {
            newUrl = __namespace__.$api.blockUser + domain;
        }
        try {
            chrome.tabs.update(tabId, { url: newUrl });
        } catch (e) {
            chrome.tabs.update({ url: newUrl });
        }
        __namespace__.$browser.updateButton(2);
    },
    onMessage: function(request, sender, sendResponse) {
        switch (request.message) {
            case 'get_white':
                {
                    sendResponse(__namespace__.$adult.white);
                }
                break;
            case 'get_black':
                {
                    var black = __namespace__.$adult.black.concat(__namespace__.$adult.blackWords);
                    sendResponse(black);
                }
                break;
            case 'remove_from_list':
                {
                    cat = request.cat;
                    if (cat == "trust")
                        __namespace__.$adult.removeWhite(request.url);
                    else
                        __namespace__.$adult.removeBlack(request.url);

                }
                break;
            case 'iswhite':
                {
                    url = request.url;
                    sendResponse(__namespace__.$adult.isWhiteSite(url));
                }
                break;
            case 'white_list':
                {
                    __namespace__.$adult.removeBlack(request.url);
                    __namespace__.$adult.addWhite(request.url);
                    if (__namespace__.$settings.options.account.require === 1) {
                        try {
                            //I have no idea what this is used for. It causes crashes. "Error in event handler for runtime.onMessage: TypeError: parentalcontrol.$browser.checkPass is not a function"
                            __namespace__.$browser.checkPass();
                        } catch(ex) {}
                    }
                    sendResponse(1);
                    if (request.tab) {
                        chrome.tabs.reload(request.tab);
                    }
                }
                break;
            case 'black_list':
                {
                    __namespace__.$adult.removeWhite(request.url);
                    __namespace__.$adult.addBlack(request.url);
                    if (request.tab) {
                        chrome.tabs.reload(request.tab);
                    }
                }
                break;
            case 'black_keyword':
                {
                    __namespace__.$adult.addBlackWords(request.keyword);
                }
                break;
            case 'isblack':
                {
                    url = request.url;
                    sendResponse(__namespace__.$adult.isBlackSite(url));
                }
                break;
            case 'keywords':
                {
                    keywords = request.value;
                    url = request.url;
                    request.tabId = sender.tab.id;
                    if (__namespace__.$adult.isWhiteSite(url) || keywords.length === 0)
                        return;
                    if (__namespace__.$settings.options.filters.$block == 1) {
                        try {
                            // This blackWords variable is not defined and creates all sort of crashes. 
                            // Amgad should check this out. I have no idea what this is used for. try/catching it for now.
                            // "background.html:1 Error in event handler for runtime.onMessage: ReferenceError: blackWords is not defined at onMessage (chrome-extension://djlmpefmljbfloddhlmhmjojaonpjekk/js/adult.js:261:33)"
                            keywords += blackWords.toString();
                        } catch(ex) {}
                    }
                    var matching = __namespace__.$utils.filterItem(__namespace__.$api.controller.adult, "words", keywords, true);
                    if (matching.value.length > 0) {
                        __namespace__.$adult.matchingWords = __namespace__.$adult.matchingWords.concat(matching.value);
                        __namespace__.$adult.matchingWords = __namespace__.$adult.matchingWords.unique();
                    }
                    if (__namespace__.$adult.matchingWords.length > 1) {
                        __namespace__.$adult.blockSite(request);
                    }
                }
                break;
            case 'blocksite':
                {
                    url = request.url;
                    request.tabId = sender.tab.id;
                    if (__namespace__.$adult.isWhiteSite(url))
                        return;
                    __namespace__.$adult.blockSite(request);
                    console.log("110.blocksite.reason:" + request.reason);
                }
                break;
            case 'scanlink':
                {
                    isPorn = __namespace__.$adult.isPornSite(request);
                    issuspicious = __namespace__.$adult.isSuspiciousDomain(request) || __namespace__.$adult.isSuspiciousWord(request.label);
                    sendResponse({
                        message: "scanresponse",
                        linkIndex: request.linkIndex,
                        anchor: request.anchor,
                        isPorn: isPorn,
                        issuspicious: issuspicious
                    });
                }
                break;

        }
    },
    onBeforeRequest: function(request) {
        var url = request.url;
        var tabId = request.tabId;
        var domain = __namespace__.$utils.getDomain(url);
        if (__namespace__.$settings.options.forcesafe.youtube === 1 &&
            domain.indexOf("youtube.com") !== -1) {
            chrome.cookies.getAll({ url: "http://*.youtube.com" },
                __namespace__.$adult.checkCookies
            );
        }
    },
    onBeforeNavigate: function(request) {
        __namespace__.$adult.matchingWords = [];
        var url = request.url;
        if (__namespace__.$adult.isPornSite(request) || url.indexOf(__namespace__.$api.blockHost) !== -1) {
            __namespace__.$adult.blockSite(request, "native");
        }
        if (__namespace__.$adult.isBlackSite(url)) {
            __namespace__.$adult.blockSite(request, "user");
        }
    },
    onCommitted: function(request) {
        var url = request.url;
        if (__namespace__.$adult.isPornSite(request) || url.indexOf(__namespace__.$api.blockHost) !== -1) {
            __namespace__.$adult.blockSite(request, "native");
        }
        if (__namespace__.$adult.isBlackSite(url)) {
            __namespace__.$adult.blockSite(request, "user");
        }
        var tabId = request.tabId;
        if (!__namespace__.$api.checkUrl(url))
            return;
        var domain = __namespace__.$utils.getDomain(url);
        var hook = (url.indexOf("?") !== -1 || url.indexOf("#") !== -1) ? "&" : "?";
        var modify = false;
        if (__namespace__.$settings.options.forcesafe.google === 1 && domain.indexOf("google") !== -1) {
            if (url.indexOf("tbm=isch") !== -1) {
                modify = true;
            } else
            if (url.indexOf("tbm=vid") !== -1) {
                modify = true;
            } else
            if (url.match(/(?=.search\?)(?=.*q=)/g)) {
                modify = true;
            } else {
                return { cancel: false };
            }
            if (modify) {
                if (url.indexOf("safe=active") === -1 && url.indexOf("safe%3Dactive") === -1) {
                    newurl = url + hook + "safe=active";
                    newurl = newurl.replace("sa=X", "");
                    chrome.tabs.update(tabId, {
                        url: newurl
                    });
                }
            }
        }
        if (__namespace__.$settings.options.forcesafe.yahoo === 1 && domain.indexOf("yahoo") !== -1) {
            if (url.indexOf("google.") !== -1) {
                return { cancel: false };
            }
            if (url.indexOf("ads") === -1 &&
                url.indexOf("p=") === -1 &&
                url.indexOf("vm=r") === -1) {
                return { cancel: false };
            }
            if (url.indexOf("images.search.yahoo.") !== -1) {
                if (url.indexOf("view?") === -1 &&
                    url.indexOf("view;_ylt") === -1) {
                    modify = true;
                }
            } else
            if (url.indexOf("video.search.yahoo.") !== -1) {
                if (url.indexOf("action=view") === -1 &&
                    url.indexOf("viewer?" === -1)) {
                    modify = true;
                }
            } else
            if (url.indexOf("search.yahoo.com") !== -1 && (url.indexOf("search;") !== -1 || url.indexOf("search?") !== -1)) {
                modify = true;
            }
            if (modify) {
                if (url.indexOf("vm=r") === -1) {
                    newurl = url + hook + "vm=r";
                    chrome.tabs.update(tabId, {
                        url: newurl
                    });
                }
            }
        }

        if (__namespace__.$settings.options.forcesafe.bing === 1 && domain.indexOf("bing") !== -1) {
            if ((url.indexOf("bing.com/search") != -1 ||
                    url.indexOf("bing.com/images") != -1 ||
                    url.indexOf("bing.com/videos") !== -1 ||
                    url.indexOf("bing.com/settings") !== -1) &&
                url.indexOf("/images/landing/settings") === -1 &&
                url.indexOf("adlt=strict") === -1) {
                hook = (url.indexOf("?") !== -1) ? "&" : "?";
                newurl = url + hook + "adlt=strict";
                url = newurl.replace("is_child=0", "is_child=1");
                chrome.tabs.update(tabId, {
                    url: newurl
                });
            }
        }
    },
    onHeadersReceived: function(details) {
        var headers = details.responseHeaders;
        var blocked = false;
        var domain = __namespace__.$utils.getDomain(details.url);
        if (__namespace__.$settings.options.forcesafe.youtube === 1) {
            if (domain.indexOf("youtube.com") !== -1) {
                chrome.cookies.getAll({ url: "http://*.youtube.com" },
                    __namespace__.$adult.checkCookies
                );
            }
        }
        if (domain.indexOf("maps.gstatic.com") === -1 &&
            domain.indexOf("google") === -1 &&
            domain.indexOf("ggpht.com") === -1) //domain.indexOf(__namespace__.$adult.activeTabDomain) !== -1)
        {
            //console.log("domain=" + domain);
            for (var i = 0; i < headers.length; i++) {
                if (headers[i].name.toLowerCase() == "rating") {
                    //console.log("headers=" + headers[i].value);
                    if (headers[i].value.indexOf("RTA" !== -1)) {
                        if (__namespace__.$adult.isWhiteSite(details.url))
                            return;
                        blocked = true;
                        break;
                    }
                }
            }
            if (blocked) {
                __namespace__.$adult.blockSite(details);
                //console.log("%c RTA rating-header blocking", 'color:green');
            }
            return { responseHeaders: details.responseHeaders };
        }
    },
    onTabCreated: function(tab) {
        __namespace__.$adult.activeTabId = tab.id;
    },
    onSelectionChanged: function(tabId, changeInfo) {
        __namespace__.$adult.activeTabId = tabId;
    }
};

$(document).ready(function() {
    __namespace__.$settings.get("white", function(result) {
        if (typeof result.white !== "undefined") {
            __namespace__.$adult.white = result.white;
        }
    });
    __namespace__.$settings.get("black", function(result) {
        if (typeof result.black !== "undefined") {
            __namespace__.$adult.black = result.black;
        }
    });
    __namespace__.$settings.get("blackwords", function(result) {
        if (typeof result.blackwords !== "undefined") {
            __namespace__.$adult.blackwords = result.blackwords;
        }
    });
    chrome.extension.onMessage.addListener(__namespace__.$adult.onMessage);
    chrome.webRequest.onBeforeRequest.addListener(__namespace__.$adult.onBeforeRequest, { urls: ["<all_urls>"] }, ["blocking"]);
    chrome.webNavigation.onBeforeNavigate.addListener(__namespace__.$adult.onBeforeNavigate);
    chrome.webNavigation.onCommitted.addListener(__namespace__.$adult.onCommitted);
    chrome.webRequest.onHeadersReceived.addListener(__namespace__.$adult.onHeadersReceived, { urls: [] }, ['responseHeaders', 'blocking']);
    // Fired when a tab is switched.
    chrome.tabs.onCreated.addListener(__namespace__.$adult.onTabCreated);
    chrome.tabs.onSelectionChanged.addListener(__namespace__.$adult.onSelectionChanged);
});