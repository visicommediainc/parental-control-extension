var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var master;
var adult;
var ads;
var block;
var trust;
var google;
var yahoo;
var bing;
var suspicious;
var google_safe;
var yahoo_safe;
var bing_safe;
var youtube_safe;
var pwd_require;
var pwd_block;
var pwd_trust;

function addEntry(cat, website) {
    //There are weird race conditions in this code that makes addEntry sometimes called multiple time, which create duplicates in the list. Quickfix for now.
    if($("[data-" + cat + "='" + website + "']").length === 0) {
        var inject = '<div class="Rectangle-8 cat_' + cat + '"><div class="website-entry"><img style="width:16px;height:16px;" src="http://www.google.com/s2/favicons?domain=' + website + '"/><span style="padding-left:10px;">' + website + '</span>' +
        '</div><img class="website-image" src="../images/ic-delete.png" cat="' + cat + '" data-' + cat + '="' + website + '" data="' + website + '"/></div>';

        $('#' + cat + '-website').append(inject);
        $('.website-image').unbind().click(function(e) {
            url = $(this).attr("data");
            cat = $(this).attr("cat");
            chrome.extension.sendMessage({ message: "remove_from_list", "cat": cat, "url": url });
            reload();
        });
    }
}

function showNotification(message) {
    var popup = document.getElementById("myPopup");
    popup.innerHTML = message;
    popup.classList.toggle("show");
    hideNotification();
}

function hideNotification() {
    setTimeout(function() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }, 5000);
}

function reload() {
    var cat = $('input[type="radio"]:checked').attr("cat");
    //window.location.href = "options.html?" + cat + "=1";
    getLists();
}

function saveOptions(isBackup) {
    var master = $('#slider-master-image').attr("class") == "on-big" ? 1 : 0;
    var adult = $('#slider-adult-image').attr("class") == "on" ? 1 : 0;
    var block = $('#slider-block-image').attr("class") == "on" ? 1 : 0;
    var trust = $('#slider-trust-image').attr("class") == "on" ? 1 : 0;

    var ads = ($("#ads").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var suspicious = ($("#suspicious").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;

    var google_safe = ($("#google_safe").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var yahoo_safe = ($("#yahoo_safe").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var bing_safe = ($("#bing_safe").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var youtube_safe = ($("#youtube_safe").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;

    var google = ($("#google").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var yahoo = ($("#yahoo").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var bing = ($("#bing").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;

    var pwd_require = ($("#pwd-require").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var pwd_block = 0; //($("#pwd-block").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;
    var pwd_trust = 0; //($("#pwd-trust").attr('class').indexOf("checkbox-checked") === -1) ? 0 : 1;

    var options = {
        filters: {
            $master: master,
            $adult: adult,
            $ads: ads,
            $block: block,
            $trust: trust
        },
        forcesafe: {
            bing: bing_safe,
            google: google_safe,
            yahoo: yahoo_safe,
            youtube: youtube_safe
        },
        search: {
            bing: bing,
            google: google,
            yahoo: yahoo,
            suspicious: suspicious
        },
        account: {
            require: pwd_require,
            block: pwd_block,
            trust: pwd_trust
        }
    };
    //console.log(isBackup ? "save_backup_settings" : "save_settings");
    //console.log(options);
    chrome.runtime.sendMessage({ message: isBackup ? "save_backup_settings" : "save_settings", options: options });
    reload();
    //window.location.reload(false);
}

function enableDisablePasswordDiv() {
    if ($('#account-password').attr('class') == 'disabled') {
        $('#account-password').attr('class', '');
    } else {
        $('#account-password').attr('class', 'disabled');
    }
}

function showWelcomeMessage() {
    alert("Thank you for installing __title__");
}

function getLists() {
    $('.cat_trust').remove();
    $('.cat_block').remove();

    chrome.extension.sendMessage({ message: "get_white" }, function(white) {
        for (var i = 0; i < white.length; i++) {
            addEntry('trust', white[i]);
        }
    });
    chrome.extension.sendMessage({ message: "get_black" }, function(black) {
        for (var i = 0; i < black.length; i++) {
            addEntry('block', black[i]);
        }
    });
}

function setAdultCheckboxState() {
    $("#google").toggleClass("checkbox-checked", google === 1);
    $("#yahoo").toggleClass("checkbox-checked", yahoo === 1);
    $("#bing").toggleClass("checkbox-checked", bing === 1);
    $("#ads").toggleClass("checkbox-checked", ads === 1);
    $("#suspicious").toggleClass("checkbox-checked", suspicious === 1);
    $("#google_safe").toggleClass("checkbox-checked", google_safe === 1);
    $("#yahoo_safe").toggleClass("checkbox-checked", yahoo_safe === 1);
    $("#bing_safe").toggleClass("checkbox-checked", bing_safe === 1);
    $("#youtube_safe").toggleClass("checkbox-checked", youtube_safe === 1);
    $("#pwd-block").toggleClass("checkbox-checked", pwd_block === 1);
    $("#pwd-trust").toggleClass("checkbox-checked", pwd_trust === 1);
}

function revertToBackupOptions() {
    chrome.extension.sendMessage({ message: "get_backup_settings" }, function(backupOptions) {
        //console.log("-=-=-=-=-=-= revertToBackupOptions - BACKUP Options");
        //console.log(backupOptions);

        $('#slider-adult').removeClass("disabled");
        $('#slider-block').removeClass("disabled");
        $('#slider-trust').removeClass("disabled");

        if(backupOptions.filters.$adult === 1) {
            $('#slider-adult-image').click();
        }
        
        if(backupOptions.filters.$block === 1) {
            $('#slider-block-image').click();
        }
        
        if(backupOptions.filters.$trust === 1) {
            $('#slider-trust-image').click();
        }
    });
}

$(document).ready(function() {
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
    var block = getUrlParameter("block");
    var allow = getUrlParameter("allow");
    var adult = getUrlParameter("adult");
    var about = getUrlParameter("about");
    var message = getUrlParameter("message");
    var domain = getUrlParameter("domain");

    if (block == 1) {
        $("#tab-block").prop("checked", true);
        setTimeout(function() {
            if (domain.length > 0) {
                $('#search-block').val(domain);
                $('#search-block-button').click();
            }
        }, 500);
    } else
    if (allow == 1) {
        $("#tab-allow").prop("checked", true);
        setTimeout(function() {
            if (domain.length > 0) {
                $('#search-trust').val(domain);
                $('#search-trust-button').click();
            }
        }, 500);
    } else
    if (adult == 1) {
        $("#tab-adult").prop("checked", true);
    } else
    if (about == 1) {
        $("#tab-about").prop("checked", true);
    } else
    if (message == 1) {
        $("#tab-about").prop("checked", true);
        showWelcomeMessage();
    } else {
        $("#tab-settings").prop("checked", true);
    }
    $('#' + $('input[name=tabs]:checked').data('section')).show();

    $('input[name=tabs]').change(function() {
        $('section:visible').hide();
        $('#' + $(this).data('section')).show();
        // window.location.href = "options.html?" + $(this).attr("cat") + "=1";
    });

    $('.slider').unbind().click(function(e) {
        id = $(this).attr("id").replace("slider-", "");
        if (e.target.getAttribute("class") == "on") {
            e.target.setAttribute("class", "off");
            $('#slider-' + id + '-image').attr("class", "off");
        } else {
            e.target.setAttribute("class", "on");
            $('#slider-' + id + '-image').attr("class", "on");
        }
        val = e.target.getAttribute("class") == "on" ? 1 : 0;
        if (id === "adult") {
            adult = val;
            if (adult === 1) {
                $('.panel').removeClass('disabled');
            } else {
                $('.panel').addClass('disabled');
            }
        }
        
        if(id === "block") {
            block = val;
        }

        if(id === "trust") {
            trust = val;
        }

        chrome.runtime.sendMessage({ message: "set_option", category: "filters", name: "$" + id, value: val });
    });

    $('#slider-master').unbind().click(function(e) {
        id = $(this).attr("id").replace("slider-", "");
        if (e.target.getAttribute("class") == "on-big") {
            saveOptions(true);
            e.target.setAttribute("class", "off-big");
            $('#slider-' + id + '-image').attr("class", "off-big");
            
            if(adult === 1) {
                $('#slider-adult-image').click();
            }
            if(block === 1) {
                $('#slider-block-image').click();
            }
            if(trust === 1) {
                $('#slider-trust-image').click();
            }

            $('#slider-adult').addClass("disabled");
            $('#slider-block').addClass("disabled");
            $('#slider-trust').addClass("disabled");
            saveOptions(false);
        } else {
            e.target.setAttribute("class", "on-big");
            $('#slider-' + id + '-image').attr("class", "on-big");

            revertToBackupOptions();
        }

        val = e.target.getAttribute("class") == "on-big" ? 1 : 0;

        state = val == 1 ? "ON" : "OFF";
        showNotification("Parental Control is " + state);
        chrome.runtime.sendMessage({ message: "set_master_option", value: val });
    });

    $('#submitFeedback').unbind().click(function() {
        if ($('#message').val().length > 256) {
            alert('Too long maximum 256 characters');
            $('#message').focus();
            return false;
        }
        var http = new XMLHttpRequest();
        var url = 'https://www.mystart.com/api/send_mail_parentalctrl/index.php';
        var params = 'product=parentalctrl&message=' + encodeURIComponent($('#message').val());
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() { //Call a function when the state changes.
            if (http.readyState == 4 && http.status == 200) {
                var reponse = JSON.parse(http.responseText);
                $('#message').val('');
                alert(reponse.msg);
            }
        };
        http.send(params);
    });

    $('.set-pwd-button').unbind().click(function(e) {
        var pwd = $('#pwd').val();
        if (pwd.length === 0) {
            alert('Please enter a valid password');
            $('#pwd').focus();
            return false;
        }
        chrome.runtime.sendMessage({ message: "set_password", value: pwd }, function(response) {
            $('.set-pwd').val('');
            $('#password-set-label').html('Password is set');
            $('#password-set-label').css('color', '#1ec713');
            $('.set-pwd-button').val('Change Password');
        });
    });


    $('#search-trust-button').unbind().click(function(e) {
        var url = $('#search-trust').val();
        
        if (url.length === 0) {
            alert('Please enter a domain');
            $('#search-trust').focus();
            return false;
        } else {
            url = url.replace('http://','').replace('https://','').replace('www.','').split(/[/?#:]/)[0];
            if (!/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/.test(url)) {
                alert('invalid domain name');
                $('#search-trust').focus();
                return false;
            }
        }
        chrome.extension.sendMessage({ message: "white_list", "url": url });
        showNotification('Site added to Trust  Sites');
        reload();
    });

    $('#search-trust').unbind().keypress(function(e) {
        if (e.keyCode == 13) {
            $('#search-trust-button').click();
        }
    });

    $('#search-block-button').unbind().click(function(e) {
        var url = $('#search-block').val();
        if (url.length < 3) {
            alert('Too short minimum 3 characters');
            $('#search-block').focus();
            return false;
        } else {
            url = url.replace('http://','').replace('https://','').replace('www.','').split(/[/?#:]/)[0];
            if (!/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/.test(url)) {
                alert('invalid domain name');
                $('#search-trust').focus();
                return false;
            }
        }
        chrome.extension.sendMessage({ message: "black_list", "url": url });
        showNotification('Site added to Block List');
        reload();
    });

    $('#search-block').unbind().keypress(function(e) {
        if (e.keyCode == 13) {
            $('#search-block-button').click();
        }
    });

    $('.ratings').unbind().click(function(e) {
        reply = confirm("Do you think this Extension deserve a 5 stars?");
        if (reply) {
            document.location.href = "https://chrome.google.com/webstore/detail/parental-control/__extensionId__/reviews";
        }
    });

    $('.Rectangle-15').unbind().click(function(e) {
        reply = confirm("Do you think this Extension deserve a 5 stars?");
        alert("reply:" + reply);
    });

    $(".checkbox").unbind().click(function(e) {
        target = $(e.target);
        if (target.attr('id') == 'pwd-require') {
            enableDisablePasswordDiv();
        }
        if (target.attr('class').indexOf("checkbox-checked") === -1) {
            target.addClass("checkbox-checked");
        } else {
            target.removeClass("checkbox-checked");
        }
        saveOptions(false);
    });

    $('#validate-pwd').unbind().keypress(function(e) {
        if (e.keyCode == 13) {
            $('#validate-pwd-button').click();
        }
    });

    $('#validate-pwd-button').unbind().click(function(e) {
        chrome.extension.sendMessage({ message: "get_password" }, function(passcode) {
            var encrypted = passcode;
            var bytes = CryptoJS.AES.decrypt(encrypted.toString(), '__namespace__');
            var plain = bytes.toString(CryptoJS.enc.Utf8);
            if (plain !== $('#validate-pwd').val()) {
                alert('wrong Password!');
                $('#validate-pwd').focus();
            } else {
                $("#allowed-mode").css('display', 'block');
                $("#blocked-mode").css('display', 'none');
                $('#slider-master').removeClass('disabled');

            }
        });
    });

    chrome.extension.sendMessage({ message: "get_options" }, function(options) {
        //console.log("-=-=-=-=-=-=Options");
        //console.log(options);
        master = options.filters.$master;
        adult = options.filters.$adult;
        ads = options.filters.$ads;
        block = options.filters.$block;
        trust = options.filters.$trust;

        google = options.search.google;
        yahoo = options.search.yahoo;
        bing = options.search.bing;
        suspicious = options.search.suspicious;

        google_safe = options.forcesafe.google;
        yahoo_safe = options.forcesafe.yahoo;
        bing_safe = options.forcesafe.bing;
        youtube_safe = options.forcesafe.youtube;

        pwd_require = options.account.require;
        pwd_block = options.account.block;
        pwd_trust = options.account.trust;

        if (typeof options.pcd != 'undefined' && options.pcd !== '') {
            $('#password-set-label').html('Password is set');
            $('#password-set-label').css('color', '#1ec713');
            $('.set-pwd-button').val('Change Password');
        }
        if (pwd_require === 1) {
            $("#pwd-require").addClass("checkbox-checked");
            $("#allowed-mode").css('display', 'none');
            $("#blocked-mode").css('display', 'block');
            $('#slider-master').addClass('disabled');
            enableDisablePasswordDiv();
        }
        if (master === 1) {
            $('#slider-master-image').attr("class", "on-big");
            $('#slider-adult').removeClass("disabled");
            $('#slider-block').removeClass("disabled");
            $('#slider-trust').removeClass("disabled");
        } else {
            $('#slider-adult').addClass("disabled");
            $('#slider-block').addClass("disabled");
            $('#slider-trust').addClass("disabled");
        }

        if (adult === 1) {
            $('#slider-adult-image').attr("class", "on");
            $('.panel').removeClass('disabled');
        } else {
            $('#slider-adult-image').attr("class", "off");
            $('.panel').addClass('disabled');
        }

        if (block === 1) {
            $("#silder-block").attr("status", "on");
            $('#slider-block-image').attr("class", "on");
        }

        if (trust === 1) {
            $("#silder-trust").attr("status", "on");
            $('#slider-trust-image').attr("class", "on");
        }

        setAdultCheckboxState();
    });

    setTimeout(getLists, 300);
});