__namespace__.$utils = {
    debug: true,
    blockHost: chrome.extension.getURL('html/blocker.html?url='),
    log: function(message) {
        if (!__namespace__.$utils.debug)
            return;
        console.log(message);
    },
    bin2String: function(array) {
        var result = "";
        for (var i = 0; i < array.length; i++) {
            result += String.fromCharCode(parseInt(array[i]));
        }
        return result;
    },
    filterItem: function(obj, item, url, noTrim) {
        function filterAnItem(el) {
            var source = el;
            if (!noTrim)
                source = el.trim();
            var rx = new RegExp(source, 'gi');
            return rx.test(url);
        }
        result = obj[item].filter(filterAnItem);
        if (result.length > 0) {
            return { label: item, value: result };
        }
        return { label: "", value: [] };
    },
    getRemoteInfo: function(url, callback, type) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url + ((/\?/).test(url) ? "&" : "?") + (new Date()).getTime(), true);
        xhr.setRequestHeader('Cache-Control', 'no-cache');
        xhr.onload = function() {
            if (type === "arraybuffer") {
                var arrayBuffer = xhr.response;
                if (arrayBuffer) {
                    var byteArray = new Uint8Array(arrayBuffer);
                    callback(byteArray);
                    arrayBuffer = null;
                    byteArray = null;
                    xhr = null;
                }
            } else {
                callback(xhr.responseText);
                xhr = null;
            }
        };
        if (type === "arraybuffer")
            xhr.responseType = "arraybuffer";

        xhr.send();
    },
    getDomain: function(url) {
        if (url === null)
            return url;
        if (url.indexOf(__namespace__.$utils.blockHost) !== -1) {
            var domainMatch = url.match(/url=*([^\n]*)/);
            if (domainMatch.length === 2) {
                return domainMatch[1];
            }
        }
        var rootDomain = "";
        var match = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
        if (match !== null && match.length > 1) {
            rootDomain = match[1].split('.').slice(-2).join('.');
        }
        if (rootDomain === "" && match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            rootDomain = match[2];
        }
        return rootDomain;
    },
    fixReg: function(match) {
        if (typeof match === 'undefined') {
            return;
        }
        return new RegExp(match, "g");
    },
    parseXML: function(xmlStr) {
        var parseXml;

        if (typeof window.DOMParser != "undefined") {
            parseXml = function(xmlStr) {
                return (new window.DOMParser()).parseFromString(xmlStr, "text/html");
            };
        } else {
            console.log("No XML parser found", xmlStr);
            return null;
        }
        return parseXml(xmlStr);
    },
    asyncLoop: function(iterations, func, callback) {
        var index = 0;
        var done = false;
        var loop = {
            next: function() {
                if (done) {
                    return;
                }

                if (index < iterations) {
                    index++;
                    func(loop);

                } else {
                    done = true;
                    callback();
                }
            },

            iteration: function() {
                return index - 1;
            },

            break: function() {
                done = true;
                callback();
            }
        };
        loop.next();
        return loop;
    },
    stringToByteArray: function(str) {
        var chars = []; // char codes
        var bytes = [];
        for (var i = 0; i < str.length; ++i) {
            var code = str.charCodeAt(i);
            chars = chars.concat([code]);
            bytes = bytes.concat([code & 0xff, code / 256 >>> 0]);
        }
        return bytes;
    }
};