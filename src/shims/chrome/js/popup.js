var domain = '';
var tabId = -1;

var getDomain = function(url) {
    if (url === null)
        return url;

    var rootDomain = "";
    var match = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
    if (match !== null && match.length > 1) {
        rootDomain = match[1].split('.').slice(-2).join('.');
    }
    if (rootDomain === "" && match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
        rootDomain = match[2];
    }
    return rootDomain;
};

$(document).ready(function() {
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
    chrome.tabs.getSelected(null, function(request) {
        console.log("starts.onload.getSelected.request.id:" + request.id + "|url:" + request.url);
        tabId = request.id;
        domain = getDomain(request.url);
        $(".domain").html(domain);
        if (domain === "" || domain.length === 0) {
            $("#block").addClass("disabledDiv");
            $("#block .label").addClass("disabledLabel");
            $("#allow").addClass("disabledDiv");
            $("#allow .label").addClass("disabledLabel");
        }
    });
    chrome.extension.sendMessage({ message: "get_options" }, function(options) {
        var master = options.filters.$master;
        var pwd_require = options.account.require;
        if (pwd_require === 1) {
            $('.slider').addClass("disabledDiv");
            $("#allowed-mode").css('display', 'none');
            $("#blocked-mode").css('display', 'block');

        }
    });

    $('.slider').unbind().click(function(e) {
        id = $(this).attr("id").replace("slider-", "");
        if (e.target.getAttribute("class") == "on") {
            e.target.setAttribute("class", "off");
            $('#slider-' + id + '-image').attr("class", "off");
        } else {
            e.target.setAttribute("class", "on");
            $('#slider-' + id + '-image').attr("class", "on");
        }
        val = e.target.getAttribute("class") == "on" ? 1 : 0;
        chrome.runtime.sendMessage({ message: "set_master_option", value: val });
    });

    $('#validate-pwd').unbind().keypress(function(e) {
        if (e.keyCode == 13) {
            $('#validate-pwd-button').click();
        }
    });

    $('#validate-pwd-button').unbind().click(function(e) {
        chrome.extension.sendMessage({ message: "get_password" }, function(passcode) {
            var encrypted = passcode;
            var bytes = CryptoJS.AES.decrypt(encrypted.toString(), '__namespace__');
            var plain = bytes.toString(CryptoJS.enc.Utf8);
            if (plain !== $('#validate-pwd').val()) {
                alert('wrong Password!');
                $('$validate-pwd').focus();
            } else {
                $("#allowed-mode").css('display', 'block');
                $("#blocked-mode").css('display', 'none');
                $('.slider').removeClass("disabledDiv");
            }
        });
    });


    $("#block").unbind().click(function(e) {
        chrome.extension.sendMessage({ message: "black_list", "url": domain, tab: tabId });
    });
    $("#allow").unbind().click(function(e) {
        chrome.extension.sendMessage({ message: "white_list", "url": domain, tab: tabId });
    });
    $("#submit").unbind().click(function(e) {
        var optionsPage = chrome.extension.getURL('html/options.html?submit=1');
        chrome.tabs.create({ url: optionsPage, active: true });
    });
    $("#adult").unbind().click(function(e) {
        var optionsPage = chrome.extension.getURL('html/options.html?adult=1');
        chrome.tabs.create({ url: optionsPage, active: true });
    });
    $(".settings").unbind().click(function(e) {
        var optionsPage = chrome.extension.getURL('html/options.html');
        chrome.tabs.create({ url: optionsPage, active: true });
    });
    $("#about").unbind().click(function(e) {
        var optionsPage = chrome.extension.getURL('html/options.html?about=1');
        chrome.tabs.create({ url: optionsPage, active: true });
    });
});