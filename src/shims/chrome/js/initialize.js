__namespace__.$initialize = {
    hostName: 'com.mystart.one.newtab.' + chrome.runtime.id,
    campaign_id: "",
    user_id: "",
    getCampaignId: function(callback) {
        chrome.runtime.sendNativeMessage(__namespace__.$initialize.hostName, {
            endpoint: 'get-campaign-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    campaign_id: null
                };
            }

            callback(response);
        });
    },
    getUserId: function(callback) {
        chrome.runtime.sendNativeMessage(__namespace__.$initialize.hostName, {
            endpoint: 'get-user-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    user_id: null
                };
            }

            callback(response);
        });
    },
    getDomain: function(url) {
        if (url === null)
            return url;

        var rootDomain = "";
        var match = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
        if (match !== null && match.length > 1) {
            rootDomain = match[1].split('.').slice(-2).join('.');
        }
        if (rootDomain === "" && match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            rootDomain = match[2];
        }
        return rootDomain;
    },
    guid: function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },
    onMessageExternal: function(request, sender, callback) {
        if (request.message === 'version') {
            callback("__version__");
        }
    },
    blockSite: function() {
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, function(tabs) {
            var tab = tabs[0];
            var url = tab.url;
            domain = __namespace__.$initialize.getDomain(url);
            var optionsPage = chrome.extension.getURL('html/options.html?block=1&domain=' + domain);
            chrome.tabs.create({ url: optionsPage, active: true });
        });
    },
    allowSite: function() {
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, function(tabs) {
            var tab = tabs[0];
            var url = tab.url;
            domain = __namespace__.$initialize.getDomain(url);
            var optionsPage = chrome.extension.getURL('html/options.html?allow=1&domain=' + domain);
            chrome.tabs.create({ url: optionsPage, active: true });
        });
    },
    $init: function() {
        //console.log("broker init");
        __namespace__.$initialize.user_id = __namespace__.$initialize.guid();
        __namespace__.$initialize.campaign_id = "123";
        chrome.storage.local.get("campaign_id", function(result) {
            if (result.campaign_id)
                __namespace__.$initialize.campaign_id = result.campaign_id;
        });
        chrome.storage.local.get("user_id", function(result) {
            if (result.user_id)
                __namespace__.$initialize.user_id = result.user_id;
        });

        __namespace__.$initialize.getCampaignId(function(data) {
            if (!data.error && data.campaign_id) {
                __namespace__.$initialize.campaign_id = data.campaign_id;
                chrome.storage.local.set({ "campaign_id": __namespace__.$initialize.campaign_id });
                chrome.runtime.setUninstallURL(__namespace__.$settings.UNINSTALL_URL + __namespace__.$initialize.campaign_id, function() {});
            }
        });

        __namespace__.$initialize.getUserId(function(data) {
            if (!data.error && data.user_id) {
                __namespace__.$initialize.user_id = data.user_id;
                chrome.storage.local.set({ "user_id": __namespace__.$initialize.user_id });
            }
        });

        chrome.contextMenus.create({
            "title": "Block this site", // 
            "onclick": __namespace__.$initialize.blockSite
        });

        chrome.contextMenus.create({
            "title": "Add this site to trust list", // 
            "onclick": __namespace__.$initialize.allowSite
        });

        chrome.runtime.onInstalled.addListener(function(details) {
            if (details.reason == "install") {
                setTimeout(function() {
                    __namespace__.$analytics.trackEvent("Runtime_Install", __namespace__.$initialize.campaign_id, " ", " ");
                }, 1000);
                chrome.tabs.getSelected(null, function(tab) {
                    chrome.tabs.update(tab.id, {
                        url: __namespace__.$settings.INSTALLED_URL
                    });
                });
                chrome.runtime.setUninstallURL(__namespace__.$settings.UNINSTALL_URL + __namespace__.$initialize.campaign_id, function() {});
            }
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    __namespace__.$initialize.$init();
    chrome.runtime.onMessageExternal.addListener(__namespace__.$initialize.onMessageExternal);
});