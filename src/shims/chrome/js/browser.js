__namespace__.$browser = {
    activeTracker: {},
    activeDomain: '',
    $pcd: '',
    activeSeed: function() {
        return {
            domain: "",
            online: 0,
            blocksite: 1,
            adult: 0,
            $ads: {}
        };
    },
    getKey: function() {
        return "__namespace__";
    },
    onMessage: function(request, sender, sendResponse) {
        switch (request.message) {
            case 'get_options':
                {
                    options = __namespace__.$settings.options;
                    options.pcd = __namespace__.$browser.$pcd;
                    sendResponse(options);
                }
                break;
            case 'save_settings':
                {
                    __namespace__.$settings.setOptions(request.options);
                }
                break;

            case "save_backup_settings":
                {
                    __namespace__.$settings.setBackupOptions(request.options);
                }
                break;
    
            case "get_backup_settings":
                {
                    sendResponse(__namespace__.$settings.backupOptions);
                }
                break;
            case 'set_option':
                {
                    __namespace__.$settings.setOption(request.category, request.name, request.value);
                }
                break;
            case 'set_master_option':
                {
                    for (var filter in __namespace__.$settings.options.filters) {
                        __namespace__.$settings.setOption('filters', filter, request.value);
                    }
                }
                break;
            case 'set_password':
                {
                    var enc = CryptoJS.AES.encrypt(request.value, __namespace__.$browser.getKey());
                    __namespace__.$settings.set({ passcode: enc.toString() });
                    __namespace__.$settings.get("passcode", function(result) {
                        if (typeof result.passcode === "undefined") {
                            __namespace__.$browser.$pcd = '';
                        }
                        __namespace__.$browser.$pcd = result.passcode;
                    });
                    sendResponse("password has been set");
                }
                break;
            case 'get_password':
                {
                    sendResponse(__namespace__.$browser.$pcd);
                }
                break;
            case 'isblock_css':
                {
                    sendResponse({ options: __namespace__.$settings.options, tracker: __namespace__.$browser.activeTracker[request.domain] });
                }
                break;
        }
    },
    updateButton: function(state, text) {
        if (state === 0 || typeof text == 'undefined')
            text = "";
        if (text === 0 || text === -1) text = "";
        if (typeof(text) === 'number') text = String(text);
        chrome.browserAction.setBadgeText({ text: text });
        chrome.browserAction.setIcon({ path: "../images/icons/48.png" });
    },
    findTrackerByTabId: function(tabId) {
        for (var domain in __namespace__.$browser.activeTracker) {
            var tracker = __namespace__.$browser.activeTracker[domain];
            if (tracker && tracker.tabIds && tracker.tabIds.indexOf(tabId) !== -1) {
                return tracker;
            }
        }
        return;
    },
    isBlockingPage: function(url) {
        if (!__namespace__.$api.checkUrl(url))
            return false;
        return false;
    },
    onSelectionChanged: function(tabId, changeInfo) {
        chrome.tabs.get(tabId, function(tab) {
            if (!tab || !__namespace__.$api.checkUrl(tab.url)) {
                __namespace__.$browser.updateButton(0);
                return;
            } else {
                var domain = __namespace__.$utils.getDomain(tab.url);
                __namespace__.$browser.activeDomain = domain;
                var item = __namespace__.$browser.activeTracker[domain];
                if (item) {
                    if (item.adult === 1) {
                        __namespace__.$browser.updateButton(2);
                    } else if (item.online === 1) {
                        __namespace__.$browser.updateButton(1);
                    } else {
                        __namespace__.$browser.updateButton(0);
                    }
                    return;
                }
                item = jQuery.extend({}, __namespace__.$browser.activeSeed());
                item.domain = domain;
                item.tabIds = [];
                item.tabIds.push(tabId);
                item.tabIds = item.tabIds.unique();
                __namespace__.$browser.activeTracker[domain] = item;
                if (__namespace__.$browser.isBlockingPage(tab.url)) {
                    request = {};
                    request.url = tab.url;
                    request.tabId = tab.id;
                    __namespace__.$browser.blockSite(request);
                    __namespace__.$browser.updateButton(2);
                } else {
                    __namespace__.$browser.updateButton(0);
                }
            }
        });
    },
    onTabUpdated: function(tabId, changeInfo, tab) {
        if (!tab || !__namespace__.$api.checkUrl(tab.url)) {
            __namespace__.$browser.updateButton(0);
            return;
        }
        if (tab.openerTabId) {
            tabId = tab.openerTabId;
        }
        var tracker = __namespace__.$browser.findTrackerByTabId(tabId);
        if (!tracker && !changeInfo.url) {
            domain = __namespace__.$utils.getDomain(tab.url);
            __namespace__.$browser.activeDomain = domain;
            item = __namespace__.$browser.activeTracker[domain];
            if (item && tab.openerTabId) {
                item.tabIds.push(tab.openerTabId);
                item.tabIds = item.tabIds.unique();
                __namespace__.$browser.activeTracker[domain] = item;
                __namespace__.$browser.updateButton(1);
                return;
            }
            item = jQuery.extend({}, __namespace__.$browser.activeSeed());
            item.domain = domain;
            item.tabIds = [];
            item.tabIds.push(tabId);
            item.tabIds = item.tabIds.unique();
            item.tabIds = item.tabIds.unique();
            __namespace__.$browser.activeTracker[domain] = item;
        }
        if (changeInfo.url) {
            domain = __namespace__.$utils.getDomain(changeInfo.url);
            __namespace__.$browser.activeDomain = domain;
            item = __namespace__.$browser.activeTracker[domain];
            if (item !== tracker) {
                if (item) {
                    item.tabIds.push(tab.id);
                    item.tabIds = item.tabIds.unique();
                    __namespace__.$browser.activeTracker[domain] = item;
                    __namespace__.$browser.updateButton(1);
                    return;
                } else {
                    item = jQuery.extend({}, __namespace__.$browser.activeSeed());
                    item.domain = domain;
                    item.tabIds = [];
                    item.tabIds.push(tab.id);
                    item.tabIds = item.tabIds.unique();
                    __namespace__.$browser.activeTracker[domain] = item;
                }
            }
        }
    },
    onRemoved: function(tabId) {
        var tracker = __namespace__.$browser.findTrackerByTabId(tabId);
        if (tracker) {
            var index = tracker.tabIds.indexOf(tabId);
            if (index !== -1) {
                if (tracker.tabIds.length === 1) {
                    delete __namespace__.$browser.activeTracker[tracker.domain];
                } else {
                    tracker.tabIds.splice(index, 1);
                }
            }
        }
    }
};

$(document).ready(function() {
    chrome.extension.onMessage.addListener(__namespace__.$browser.onMessage);
    // Fired when a tab is switched.
    chrome.tabs.onSelectionChanged.addListener(__namespace__.$browser.onSelectionChanged);
    chrome.tabs.onUpdated.addListener(__namespace__.$browser.onTabUpdated);
    chrome.tabs.onRemoved.addListener(__namespace__.$browser.onRemoved);
    __namespace__.$settings.get("passcode", function(result) {
        if (typeof result.passcode === "undefined") {
            __namespace__.$browser.$pcd = '';
        }
        __namespace__.$browser.$pcd = result.passcode;
    });

});