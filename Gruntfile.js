module.exports = function(grunt) {
    "use strict";
    var os = require('os'),
        osType = os.type();
    require("load-grunt-tasks")(grunt);
    var PATHS = {
        js: [
            '*.json',
            '*.js',
            'src/**/*.json',
            'src/**/*.js'
        ]
    };
    var namespace = grunt.option('namespace');
    var target = grunt.option('target');
    var browser = grunt.option('browser');
    var debug = (target == "prod") ? false : true;
    if (typeof namespace == 'undefined') {
        namespace = 'parentalcontrol';
    }
    console.log("namespace=" + namespace);

    var pkg = grunt.file.readJSON('builds/' + namespace + '/package.json');
    var version = pkg.version;
    var name = pkg.title;
    var domain = pkg.domain;
    var uninstallurl = pkg.uninstall_url;
    var short = pkg.short;
    var description = pkg.description;
    var campaign = pkg.compaignId;
    var extensionId = pkg.extensionId;
    var ga = pkg.analytics.ga;
    var piwik = pkg.analytics.piwik;
    var color = pkg.color;

    var target = grunt.option('target');
    var browser = grunt.option('browser');
    var debug = (target == "prod") ? false : true;

    if (!browser || browser.length == 0)
        browser = 'chrome';

    console.log("namespace=" + namespace);
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('builds/' + namespace + '/package.json'),
        clean: ['dist', 'temp/' + namespace, 'uglify'],
        jshint: {
            js: [
                "src/**/*.js"
            ],
            options: {
                esnext: true,
                reporter: require('jshint-summary'),
                "-W087" : true, // Mandatory to debug code properly.
                ignores: ['Gruntfile.js', 'src/**/jquery*.js', 'src/**/aes.js', 'src/**/lzma-d.js', 'src/**/piwik.lib.js'],
            },
            all: PATHS.js,
            watched: { src: [] }
        },
        sync: {
            chrome: {
                files: [
                    { cwd: 'src/shims/' + browser + '/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf', '!fonts/**/*.woff'], dest: 'dist/' + browser + '/', filter: 'isFile' },
                    { cwd: 'dist/temp/main/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf', '!fonts/**/*.woff'], dest: 'dist/' + browser + '/', filter: 'isFile' },
                ],
                updateAndDelete: false
            },
            firefox: {
                files: [
                    { cwd: 'src/shims/firefox/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf'], dest: 'dist/firefox/', filter: 'isFile' },
                    { cwd: 'dist/temp/main/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf'], dest: 'dist/firefox/data/', filter: 'isFile' },
                ],
                ignoreInDest: '**/*.rdf',
                updateAndDelete: false
            }
        },
        jpm: {
            options: {
                src: "./src/shims/firefox/",
                xpi: "./public/"
            }
        },
        copy: {
            main: {
                files: [
                    { expand: false, cwd: '', src: ['tracker.json'], dest: 'uglify/tracker_min.json', filter: 'isFile' },
                    { expand: false, cwd: '', src: ['tracker.json'], dest: 'uglify/tracker.json', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/js/', src: ['jquery-2.1.4.min.js', 'piwik.lib.js'], dest: 'temp/' + name + '/js/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/images/', src: ['**'], dest: 'temp/' + name + '/images/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/html/', src: ['**'], dest: 'temp/' + name + '/html/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/css/', src: ['**'], dest: 'temp/' + name + '/css/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/fonts/', src: ['**'], dest: 'temp/' + name + '/fonts/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/', src: ['manifest.json'], dest: 'temp/' + name + '/', filter: 'isFile' },
                    { expand: true, cwd: 'builds/' + namespace + '/', src: ['**.png'], dest: 'temp/' + name + '/images/', filter: 'isFile' },
                    { expand: true, cwd: 'builds/' + namespace + '/icons/', src: ['**'], dest: 'temp/' + name + '/images/icons/', filter: 'isFile' },
                ]
            }
        },
        uglify: {
            dev: {
                options: {
                    banner: '/*! ' + name + ' V<%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>) */\n',
                    mangle: false,
                    compress: false,
                    beautify: true
                },
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/js',
                    src: ['*.js', '!piwik.lib.js', '!jquery-*.js'],
                    dest: 'temp/' + name + '/js',
                }]
            },
            prod: {
                options: {
                    banner: '/*! ' + name + ' V<%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>) */\n',
                    mangle: true,
                    compress: true,
                    beautify: false
                },
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/js',
                    src: ['*.js', '!piwik.lib.js', '!jquery-*.js'],
                    dest: 'temp/' + name + '/js',
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'temp/' + name + '/css',
                    ext: '.css'
                }]
            }
        },
        zip: {
            chrome: {
                cwd: 'temp/' + name + '/',
                src: ['temp/' + name + '/**/*', ],
                dest: 'public/' + short + '_' + version + '.zip'
            },
            firefox: {
                cwd: 'temp/' + name + '/',
                src: ['temp/' + name + '/**/*', ],
                dest: 'public/' + short + '_' + version + '.xpi'
            }
        },
        'string-replace': {
            dist: {
                files: [{
                        expand: true,
                        cwd: 'src/shims/' + browser + '/',
                        src: ['**/manifest.json'],
                        dest: 'temp/' + name + '/'
                    },
                    {
                        expand: true,
                        cwd: 'src/shims/' + browser + '/js/',
                        src: ['**/**.js', '!piwik.lib.js', '!jquery-2.1.4.min.js'],
                        dest: 'temp/' + name + '/js/'
                    },
                    {
                        expand: true,
                        cwd: 'src/shims/' + browser + '/html/',
                        src: ['**/**.html'],
                        dest: 'temp/' + name + '/html/'
                    },
                    {
                        expand: true,
                        cwd: 'src/shims/' + browser + '/css/',
                        src: ['**/**.css'],
                        dest: 'temp/' + name + '/css/'
                    },
                    {
                        expand: true,
                        cwd: 'uglify/',
                        src: ['tracker.json'],
                        dest: 'uglify/'
                    }
                ],
                options: {
                    replacements: [{
                            pattern: /__name__/g,
                            replacement: name
                        },
                        {
                            pattern: /__version__/g,
                            replacement: version
                        },
                        {
                            pattern: /__color__/g,
                            replacement: color
                        },
                        {
                            pattern: /__short__/g,
                            replacement: short
                        },
                        {
                            pattern: /__namespace__/g,
                            replacement: namespace
                        },
                        {
                            pattern: /__domain__/g,
                            replacement: domain
                        },
                        {
                            pattern: /__uninstallurl__/g,
                            replacement: uninstallurl
                        },
                        {
                            pattern: /__debug__/g,
                            replacement: debug
                        },
                        {
                            pattern: /__description__/g,
                            replacement: description
                        },
                        {
                            pattern: '__ga__',
                            replacement: ga
                        },
                        {
                            pattern: '__piwik__',
                            replacement: piwik
                        },
                        {
                            pattern: /__title__/g,
                            replacement: name
                        },
                        {
                            pattern: /__extensionId__/g,
                            replacement: extensionId
                        },
                        {
                            pattern: '__date__',
                            replacement: grunt.template.today("yyyy-mm-dd HH:MM")
                        }
                    ]
                }
            }
        },
        'regex-check': {
            files: "tracker.json",
            options: {
                pattern: /match/g
            },
        },
        'concat': {
            options: {
                separator: '-------------',
            },
            dist: {
                src: ['list/sites.list', 'list/words.list', 'list/domains.list', 'list/ads.list', 'list/social.list', 'list/analytics.list', 'list/sponsors.list', 'list/spyware.list'],
                dest: 'list/all.list',
            },
        },
        'jsObfuscate': {
            test: {
                options: {
                    concurrency: 2,
                    keepLinefeeds: false,
                    keepIndentations: false,
                    encodeStrings: true,
                    encodeNumbers: true,
                    moveStrings: true,
                    replaceNames: true,
                    variableExclusions: ['^_get_', '^_set_', '^_mtd_']
                },
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/js',
                    src: ['*.js', '!piwik.lib.js', '!jquery-*.js'],
                    dest: 'temp/' + name + '/js'
                }]

            }
        },
        javascript_obfuscator: {
            options: {
                compact: true,
                controlFlowFlattening: false,
                deadCodeInjection: false,
                debugProtection: false,
                debugProtectionInterval: false,
                disableConsoleOutput: false,
                mangle: true,
                rotateStringArray: true,
                selfDefending: true,
                stringArray: true,
                stringArrayEncoding: false,
                stringArrayThreshold: 0.75,
                unicodeEscapeSequence: false
            },
            main: {
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/js',
                    src: ['*.js', '!piwik.lib.js', '!jquery-*.js'],
                    dest: 'temp/' + name + '/js'
                }]
            }
        },
        'exec': {
            tool: {
                command: 'brotli.exe --in list/all.list --out list/all.list.br --force'
            }
        }
    });
    grunt.loadNpmTasks('js-obfuscator');


    // Default task(s).
    grunt.registerTask('no-watch', ['clean', 'jshint:all', 'sync:' + browser]);
    grunt.registerTask('firefox', ['copy', 'string-replace', 'uglify:' + ((target === "prod") ? "prod" : "dev"), 'jsonmin', 'zip:firefox', 'json-minify']);
    grunt.registerTask('chrome', ['copy', 'string-replace', 'uglify:' + ((target === "prod") ? "prod" : "dev"), 'cssmin', target == "prod" ? "javascript_obfuscator" : "zip:chrome", 'zip:chrome']);
    grunt.registerTask('default', ['no-watch', browser]);
};